<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
        <title>Workora - Finding a professional will never be a hassle again</title>
        <meta name="description" content="Find professionals and professional jobs on Workora">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Workora">

        <!-- icons -->
        	<!-- std -->
        	<link rel="shortcut icon" href="img/favicon.png">
        	<!-- retina ipad-->
        	<link rel="apple-touch-icon" sizes="144x144" href="img/favicon-retina-ipad.png">
        	<!-- retina iphone-->
        	<link rel="apple-touch-icon" sizes="114x114" href="img/favicon-retina-iphone.png">
        	<!-- std ipad--> 
        	<link rel="apple-touch-icon" sizes="72x72" href="img/favicon-standard-ipad.png">
        	<!-- std iphone--> 
        	<link rel="apple-touch-icon" sizes="57x57" href="img/favicon-standard-iphone.png">
		<!-- ../icons -->


        <!-- custom css -->
        	<link rel="stylesheet" href="{{ URL::to('/')}}/css/style-2.css" />
        	<link rel="stylesheet" href="{{ URL::to('/')}}/css/custom.css" />
        	<link rel="stylesheet" href="{{ URL::to('/')}}/css/jquery.mCustomScrollbar.css">
		<!-- custom css -->
		
		<!-- modernizer -->
		<script src="{{ URL::to('/')}}/js/modernizr.js"></script>
	</head>
	
	<body>
		<!-- page loader -->
			<div class="loading-part animated-quick">
				<div class="loader-part opacity-0 animated-quick">
					<div class="loader-circle"></div>
					<div class="loader-text">Workora - Finding a professional will never be a hassle again</div>
				</div>
			</div>
		<!-- ../page loader -->

		<!-- navigation -->
			<nav class="navbar navbar-default navbar-fixed-top" id="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							@if (Auth::guest())
								<li><a href="" data-toggle="modal" data-target="#myModal">Login</a></li>
								<li><a href="{{ URL::to('/')}}/auth/register">Register</a></li>
							@else
								
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ URL::to('/')}}/users">Profile</a></li>
										<li><a href="{{ URL::to('/')}}/users/settings">Settings</a></li>
										<li><a href="{{ URL::to('/')}}/auth/logout">Logout</a></li>
									</ul>
								</li>
							@endif
						</ul>
					</div>
				</div>
			</nav>
		<!-- ../navigation -->

		@yield('content')

		<!-- login modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document" id="contact-content">
					<div class="modal-content">			      

					    <div class="modal-body" id="contact-content">
							<div class="panel-body">	
								<form id="contact-form" class="form-horizontal" role="form" method="POST" action="{{ URL::to('/')}}/auth/login">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">

									<div class="form-group">
										<label class="col-md-4 control-label" style="color: #fff;">E-Mail Address</label>
										<div class="col-md-6">
											<input type="email" class="form-control" name="email" value="">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label" style="color: #fff;">Password</label>
										<div class="col-md-6">
											<input type="password" class="form-control" name="password">
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<div class="checkbox">
												<label style="color: #fff;">
													<input type="checkbox" name="remember"> Remember Me
												</label>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<a href="{{ URL::to('/')}}/password/email">Forgot Your Password?</a>
										</div>
									</div>
							</div>
					    </div>
					      
					  	<div class="modal-footer" id="contact-content">
					  		<button type="submit" class="btn btn-color"> Login</button>
					    </form>		
					    	<a href="http://localhost:8000/auth/register" class="btn btn-color">Register</a>
						</div>
					</div>
			  </div>
			</div>
		<!-- ../login modal -->

		<!-- register modal -->
			<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
				<div class="modal-dialog" role="document" id="contact-content">
					<div class="modal-content">			      

					    <div class="modal-body" id="contact-content">
							<div class="panel-body">	
								<form id="contact-form" class="form-horizontal" role="form" method="POST" action="/auth/login">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">

									<div class="form-group">
										<label class="col-md-4 control-label" style="color: #fff;">E-Mail Address</label>
										<div class="col-md-6">
											<input type="email" class="form-control" name="email" value="">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label" style="color: #fff;">Password</label>
										<div class="col-md-6">
											<input type="password" class="form-control" name="password">
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<div class="checkbox">
												<label style="color: #fff;">
													<input type="checkbox" name="remember"> Remember Me
												</label>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<a href="/password/email">Forgot Your Password?</a>
										</div>
									</div>
							</div>
					    </div>
					      
					  	<div class="modal-footer" id="contact-content">
					  		<button type="submit" class="btn btn-color"> Login</button>
					    </form>		
					    	<a href="http://localhost:8000/auth/register" class="btn btn-color">Register</a>
						</div>
					</div>
			  </div>
			</div>
		<!-- ../register modal -->

	<!-- scripts-->
	<script src="{{ URL::to('/')}}/js/jquery.min.js"></script>
	<script src="{{ URL::to('/')}}/js/jquery.easings.min.js"></script>
	<script src="{{ URL::to('/')}}/js/bootstrap.min.js"></script>
	<script src="{{ URL::to('/')}}/js/jquery.swipebox.js"></script>
	<script src="{{ URL::to('/')}}/js/jquery.mousewheel.js"></script>
	<script src="{{ URL::to('/')}}/js/vegas.js"></script>	
	
	<!-- main js-->
	<script src="{{ URL::to('/')}}/js/main.js"></script>
	
	<!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->
	</body>
</html>