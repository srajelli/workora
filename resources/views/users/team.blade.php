@extends('users.app-user')

@section('content')
<div class="container">
	<div class="col-md-12">
  		<h3>My Team</h3>
  		<hr>

		<div class="panel panel-default">
			<div class="panel-body">
				<p>Please start a job to see your team.</p>
			</div>
		</div>  				
	</div>
</div>
@endsection
