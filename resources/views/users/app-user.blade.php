<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
        <title>Workora - Finding a professional will never be a hassle again</title>
        <meta name="description" content="Find professionals and professional jobs on Workora">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Workora">

        <!-- icons -->
        	<!-- std -->
        	<link rel="shortcut icon" href="img/favicon.png">
        	<!-- retina ipad-->
        	<link rel="apple-touch-icon" sizes="144x144" href="img/favicon-retina-ipad.png">
        	<!-- retina iphone-->
        	<link rel="apple-touch-icon" sizes="114x114" href="img/favicon-retina-iphone.png">
        	<!-- std ipad--> 
        	<link rel="apple-touch-icon" sizes="72x72" href="img/favicon-standard-ipad.png">
        	<!-- std iphone--> 
        	<link rel="apple-touch-icon" sizes="57x57" href="img/favicon-standard-iphone.png">
		<!-- ../icons -->


        <!-- custom css -->
        	<link rel="stylesheet" href="{{ URL::to('/')}}/css/style-2.css" />
        	<link rel="stylesheet" href="{{ URL::to('/')}}/css/custom.css" />
        	<link rel="stylesheet" href="{{ URL::to('/')}}/css/jquery.mCustomScrollbar.css">
		<!-- custom css -->
		
		<!-- modernizer -->
		<script src="{{ URL::to('/')}}/js/modernizr.js"></script>
	</head>
<style type="text/css">
	h2 {
		font-family: 'Open Sans', sans-serif;
	}

	.navbar, .jumbotron {
		margin-bottom: 0px;
	}

	.conta {
		align-items: center;
	  	display: flex;
	  	justify-content: center;
	}

	.footer {
		background-color: #eee;
		margin-top: 30px;
		padding-top: 10px;
		padding-bottom: 30px;
	}
	.footer h3 {
		font-size: 15px;
		font-weight: 700;
		padding-top: 16px;
  		margin-bottom: 12px;
	}

	.link-list {
		list-style: none;
  		margin: 0;
	  	padding: 0;
	}

	a, a:hover{
		color: #333;
	}

	}
</style>
<body>
	<nav class="navbar navbar-default" id="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Workora</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ URL::to('/')}}/users/jobs">Jobs</a></li>
					<li><a href="{{ URL::to('/')}}/users/team">Team</a></li>
					<li><a href="{{ URL::to('/')}}/users/messages">Messages</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="" data-toggle="modal" data-target="#myModal">Login</a></li>
						<li><a href="{{ URL::to('/')}}/auth/register">Register</a></li>
					@else
						<li><a href="#"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span></a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ URL::to('/')}}/users">Profile</a></li>
								<li><a href="{{ URL::to('/')}}/users/settings">Settings</a></li>
								<li><a href="{{ URL::to('/')}}/auth/logout">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
			<div class="modal-content">
			      <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal">x</button>
			            <h3 class="text-center">Login</h3>
			      </div>
			      <div class="modal-body">
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="/auth/login">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<a href="{{ URL::to('/')}}/password/email">Forgot Your Password?</a>
							</div>
						</div>
					
				</div>
			      </div>
			      
			  		<div class="modal-footer">
			  			<button type="submit" class="btn btn-default"> Login</button>
			    	</form>	
			    		<a href="{{ URL::to('/')}}/auth/register" class="btn btn-default">Register</a>
			      	</div>
			</div>
	  </div>
	</div>

	<section id="footer" class="section active-section">
			<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3 class="small-text uppercase-text strong-text">Workora Internet</h3>
					<p>Copyright (c) 2015 Workora Internet Pvt Ltd. All Rights Reserved.</p>
				</div>

				<div class="col-md-2 col-md-offset-2">
					<h3 class="small-text uppercase-text strong-text">About</h3>
					<ul class="link-list">
						<li><a href="#" title="">We are hiring!</a></li>
						<li><a href="#" title="">Legal</a></li>
						<li><a href="#" title="">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-2">
				<h3 class="small-text uppercase-text strong-text">Support</h3>
					<ul class="link-list">
						<li><a href="#" title="">Pricing</a></li>
						<li><a href="#" title="">Refunds</a></li>
						<li><a href="#" title="">Security</a></li>
					</ul>
				</div>

				<div class="col-md-2">
					<h3 class="small-text uppercase-text strong-text">Follow</h3>
					<ul class="link-list">
						<li><a href="#" title="">Facebook</a></li>
						<li><a href="#" title="">Twitter</a></li>
						<li><a href="#" title="">GooglePlus</a></li>
					</ul>
				</div>
			</div>
			</div>
	</section>

	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
</body>
</html>
