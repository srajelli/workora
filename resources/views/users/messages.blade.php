@extends('users.app-user')

@section('content')
<div class="container">
	<div class="col-md-12">
  		<h3>Messages</h3>
  		<hr>
		
		<div class="panel panel-default">
			<div class="panel-body">
				<p>You have no messages.</p>
			</div>
		</div>  				
	</div>
</div>
@endsection
