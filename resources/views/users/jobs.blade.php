@extends('users.app-user')

@section('content')
<div class="container">
	<div class="col-md-12">
		<div class="panel-body" style="padding:0px;">
			<div class="col-md-3">
				<h3 style="margin-top:15px;">Open Jobs</h3>	  			
			</div>	
			
			<div class="col-md-9">
				<a href="{{ URL::to('/')}}/users/jobs/post" class="btn btn-color" style="float: right;margin-top: 12px;   margin-bottom: 18px;">Post a Job</a>	  			
			</div>								
		</div>
		<hr style="margin-top:0px;">

		<div class="panel panel-default">
			<div class="panel-body">
				<ul class="list-group">
				<?php
						foreach ($data as $ojob) 
						{
							echo "<li class=\"list-group-item\">";
							echo $ojob->title;	
							echo "</li>";
						}
				?>
				</ul>				
			</div>
		</div>
	</div>
</div>	
@endsection