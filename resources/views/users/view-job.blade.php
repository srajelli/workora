@extends('users.app-user')

@section('content')
<div class="container">
	<div class="col-md-12">
		<div class="panel-body" style="padding:0px;">
			<div class="col-md-2">
				<h3>Post a Job</h3>	  			
			</div>	
			
			<div class="col-md-9">
				<a href="{{ URL::to('/')}}/users/jobs" class="btn btn-default" style="float: right;margin-top: 12px;   margin-bottom: 18px;">Back to Open Jobs</a>	  			
			</div>								
		</div>
		<hr style="margin-top:0px;">

		<div class="panel panel-default">
			<div class="panel-body">
				<h4>Describe The Job</h4>
				<br>
				<form action="{{URL::to('/')}}/users/jobs/post" method="POST">
				<div class="form-group">
    				<label for="exampleInputEmail1">Name your Job posting</label><br	
    				<span>This is the first thing contractors will see so keep it clear and consise</span>
    				
    				<input type="text" class="form-control" name="job-title" disabled="">
  				</div>	

				<div class="form-group">
    				<label for="exampleInputEmail1">Some short but clear description</label>
    				<textarea name="job-desc" class="form-control" rows="3" disabled="" ></textarea>
  				</div>

				<div class="form-group">
    				<label for="exampleInputEmail1">Select an expertise level</label>
    				<br>
    				<span>Proving an expertise level will show your listing to only the one's who fit the category</span>
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Talented and Aspiring 
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Talented well experienced
						  </label>
						</div>    				
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Experienced and a Ninja
						  </label>
						</div>    					
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Veteran who knows his game
						  </label>
						</div>    													
  				</div>

				<div class="form-group">
    				<label for="exampleInputEmail1">Provide us a budget</label>
						<div class="form-group">
						    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
						    <div class="input-group">
						      <div class="input-group-addon">&#8377</div>
						      <input type="text" class="form-control" name="job-budget" placeholder="Amount" disabled="">
						      <div class="input-group-addon">.00</div>
						    </div>
						</div>
  				</div>

				<div class="form-group">
    				<label for="exampleInputEmail1">Scheduled Dead line</label>	    				
    				<input type="date" class="form-control" name="dead-line" disabled="">
  				</div>	
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
  				<div class="colarado" style="padding-top:45px;">
					<div class="col-md-9">
						<label><input name="auto-unlist" type="checkbox" value="1"> Automatically unlist this post if inactive for more than 14 days. </label>
					</div>	
						
					<div class="col-md-3">
						<button type="submit" class="btn btn-default" style="float: right;">Post it now</button>	  			
					</div>		  					
				</form>
  				</div>
			</div>
					
		</div>
	</div>
</div>	
@endsection