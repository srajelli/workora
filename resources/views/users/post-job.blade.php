@extends('users.app-user')

@section('content')
<div class="container">
	<div class="col-md-12">
		<div class="panel-body" style="padding:0px;">
			<div class="col-md-3">
				<h3 style="margin-top:20px;">Post a Job</h3>	  			
			</div>	
			
			<div class="col-md-9">
				<a href="{{ URL::to('/')}}/users/jobs" class="btn btn-color" style="float: right;margin-top: 12px;   margin-bottom: 18px;">Back to Open Jobs</a>	  			
			</div>								
		</div>
		<hr style="margin-top:0px;">

<div class="panel panel-default">
			<div class="panel-body">
				<h4>Describe The Job</h4>
				<br>
				<form action="http://localhost:8000/users/jobs/post" method="POST">
				<div class="form-group">
    				<label for="exampleInputEmail1" style="
    padding-bottom: 10px;
">Name your Job posting</label><input type="text" class="form-control" name="job-title" placeholder="Professional Wordpress Engineer experienced with Child Theming and Plugin Development needed">
  				</div>	

				<div class="form-group">
    				<label for="exampleInputEmail1" style="
    padding-bottom: 10px;
">Some short but clear description</label>
    				<textarea name="job-desc" class="form-control" rows="3" placeholder="Try to explain what work you need to be done, you have 1000 charchter limit so try covering everything relevant."></textarea>	
  				</div>

				<div class="form-group">
    				<label for="exampleInputEmail1" style="
    padding-bottom: 10px;
">Select an expertise level</label>
    				<br>
    				
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Talented and Aspiring 
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Talented well experienced
						  </label>
						</div>    				
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Experienced and a Ninja
						  </label>
						</div>    					
						<div class="radio">
						  <label>
						    <input name="exp" type="radio" value="1">
						    Veteran who knows his game
						  </label>
						</div>    													
  				</div>

				<div class="form-group">
    				<label for="exampleInputEmail1" style="
    padding-bottom: 10px;
">Provide us a budget</label>
						<div class="form-group">
						    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
						    <div class="input-group">
						      <div class="input-group-addon">₹</div>
						      <input type="text" class="form-control" name="job-budget" placeholder="Amount">
						      <div class="input-group-addon">.00</div>
						    </div>
						</div>
  				</div>

				<div class="form-group">
    				<label for="exampleInputEmail1" style="
    padding-bottom: 10px;
">Scheduled Dead line</label>	    				
    				<input type="date" class="form-control" name="dead-line" placeholder="Professional Wordpress Engineer experienced with Child Theming and Plugin Development needed">
  				</div>	
				<input type="hidden" name="_token" value="ExBbMZuicQBtprx655EZI2pXlNIQT9zBBbAZfTSK">
  				<div class="colarado" style="padding-top:45px;">
					<div class="col-md-9">
						<label><input name="auto-unlist" type="checkbox" value="1"> Automatically unlist this post if inactive for more than 14 days. </label>
					</div>	
						
					<div class="col-md-3">
						<button type="submit" class="btn btn-color" style="float: right;">Post it now</button>	  			
					</div>		  					
				
  				</div></form>
			</div>
					
		</div>
	</div>
</div>	
@endsection
