
@extends('app')

@section('content')
	<div class="bg" style="  background-image: url(http://localhost:8000/img/main-bg-min.jpg);
  background-color: rgba(0, 0, 0, 0);
  background-size: cover;
  background-position: 50% 50%;">
	
<div class="container">
	
	<div id="contact-content" class="login">

		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
			</div>
		@endif

		<form id="contact-form" class="form-signin" role="form" method="POST" action="/auth/register">
        	<h2 class="text-center form-signin-heading">Register</h2>        	
        	<input type="hidden" 				name="_token" value="{{ csrf_token() }}">
        	<input type="text" 					name="name" 					placeholder="Your Name" 			class="form-control"  value="" style="margin-bottom: 9px;">
        	<input type="tel" 					name="mob" 						placeholder="Mobile Number" 		class="form-control"  value="" style="margin-bottom: 9px;">
        	<input type="email" 				name="email" 					placeholder="Email" 				class="form-control"  value="" style="margin-bottom: 9px;">
        	<input type="password" 				name="password" 				placeholder="Password"  			class="form-control" style="margin-bottom: 9px;" required="">
        	<input type="password" 				name="password_confirmation" 	placeholder="Confirm your Password" class="form-control" required="">
        	
        	
        	<button class="btn btn-lg btn-color btn-block" type="submit">Register</button>
      	</form>
  	</div>
  </div>
</div>


	<section id="footer" class="section active-section">
			<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3 class="small-text uppercase-text strong-text">Workora Internet</h3>
					<p>Copyright (c) 2015 Workora Internet Pvt Ltd. All Rights Reserved.</p>
				</div>

				<div class="col-md-2 col-md-offset-2">
					<h3 class="small-text uppercase-text strong-text">About</h3>
					<ul class="link-list">
						<li><a href="#" title="">We are hiring!</a></li>
						<li><a href="#" title="">Legal</a></li>
						<li><a href="#" title="">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-2">
				<h3 class="small-text uppercase-text strong-text">Support</h3>
					<ul class="link-list">
						<li><a href="#" title="">Pricing</a></li>
						<li><a href="#" title="">Refunds</a></li>
						<li><a href="#" title="">Security</a></li>
					</ul>
				</div>

				<div class="col-md-2">
					<h3 class="small-text uppercase-text strong-text">Follow</h3>
					<ul class="link-list">
						<li><a href="#" title="">Facebook</a></li>
						<li><a href="#" title="">Twitter</a></li>
						<li><a href="#" title="">GooglePlus</a></li>
					</ul>
				</div>
			</div>
			</div>
	</section>

@endsection
