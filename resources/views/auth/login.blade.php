@extends('app')

@section('content')
<div class="container">
	<div id="contact-content" class="login">
		<form id="contact-form" class="form-signin">
        	<h2 class="text-center form-signin-heading">Login in below</h2>        	
        	<input type="email" placeholder="Email" class="form-control" name="email" value="" style="margin-bottom: 9px;">
        	<input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
        	
        	<div class="checkbox">
          		<label><input type="checkbox" value="remember-me">Keep me Signed in</label>
        	</div>
        	
        	<button class="btn btn-lg btn-color btn-block" type="submit">Sign in</button>
      	</form>
  </div>
</div>


	<section id="footer" class="section active-section">
			<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3 class="small-text uppercase-text strong-text">Workora Internet</h3>
					<p>Copyright (c) 2015 Workora Internet Pvt Ltd. All Rights Reserved.</p>
				</div>

				<div class="col-md-2 col-md-offset-2">
					<h3 class="small-text uppercase-text strong-text">About</h3>
					<ul class="link-list">
						<li><a href="#" title="">We are hiring!</a></li>
						<li><a href="#" title="">Legal</a></li>
						<li><a href="#" title="">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-2">
				<h3 class="small-text uppercase-text strong-text">Support</h3>
					<ul class="link-list">
						<li><a href="#" title="">Pricing</a></li>
						<li><a href="#" title="">Refunds</a></li>
						<li><a href="#" title="">Security</a></li>
					</ul>
				</div>

				<div class="col-md-2">
					<h3 class="small-text uppercase-text strong-text">Follow</h3>
					<ul class="link-list">
						<li><a href="#" title="">Facebook</a></li>
						<li><a href="#" title="">Twitter</a></li>
						<li><a href="#" title="">GooglePlus</a></li>
					</ul>
				</div>
			</div>
			</div>
	</section>

@endsection
