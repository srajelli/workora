@extends('app')

@section('content')
		<!-- home-part -->
		<section id="home-part" class="scale-max">
			<div class="overlay"></div>

			<div class="item-title text-center animated-quick">

				<h2 class="home-title">Building alliances beyond profession</h2>

				<p>60 seconds to meet your next big team</p>

				<div id="subscribe">
	                <form action="#" id="notifyMe" method="POST">
	                    <div class="form-group">
	                        <div class="controls">
	                           
	                        	<input type="text" id="mail-sub" name="email" placeholder="Write your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Write your email here'" class="form-control email srequiredField" />
	                        	<i class="fa fa-spinner opacity-0"></i>
	                            <button class="btn btn-lg submit" type="submit">I AM IN</button>
	                            <div class="clear"></div>

	                        </div>
	                    </div>
	                </form> 
        		</div>

			</div>
		</section> 
		<!-- ../home-part -->

		<!-- copyright -->
			<p class="copyright">© Workora Internet 2015</p>	
		<!-- ../copyright -->
@endsection