<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProProfile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('skill_liner');
			$table->string('skill_tags');
			$table->string('overview');
			$table->rememberToken();
			$table->timestamps();
		});
	}	

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pros');
	}

}
