<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}
	/**
	 * User Dashboard
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('users.dashboard');
	}

	public function jobs()
	{
		$data = \App\Jobs::where('status', 'active')->get();
		return view('users.jobs')->with('data',$data);
	}

	public function post(){


		return view('users.post-job');
	}

	public function view(){
		return view('users.view-job');
	}
	public function storenewpost(Request $request)
	{
		
		$jobs = new \App\Jobs;
		$jobs->title = 				$request->input('job-title');
		$jobs->desc = 				$request->input('job-desc');
		$jobs->budget = 			$request->input('job-budget');
		$jobs->level = 				$request->input('exp');
		$jobs->deadline = 			$request->input('dead-line');
		$jobs->status = 			'active';

		$jobs->save();
		//echo '<pre>';
		//return $request;
		return view('users.success');
	}

	public function team()
	{
		return view('users.team');
	}

	public function messages()
	{
		return view('users.messages');
	}	
}
