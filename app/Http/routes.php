<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/users', 'UserController@index');
Route::get('/pro', 'ProController@index');

Route::get('/users/jobs', 'UserController@jobs');
Route::get('/users/jobs/post', 'UserController@post');
Route::get('/users/jobs/view', 'UserController@view');
Route::post('/users/jobs/post', 'UserController@storenewpost');


Route::get('/users/team', 'UserController@team');
Route::get('/users/messages', 'UserController@messages');



Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',	
	'password' => 'Auth\PasswordController',
]);
